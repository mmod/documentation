# Massively Modified Documentation

README.md covers many things on a per-project basis, however, we find that we re-write documentation and wiki text quite frequently. In an attempt to centralize our documentation we have created this project.

If you like our software, please consider making a donation. Donations help greatly to maintain the Massively Modified network and continued support of our open source offerings:

[![Donate via PayPal.com](https://gitlab.com/mmod/kwaeri-user-experience/raw/master/images/mmod-donate-btn-2.png)](https://www.paypal.com/cgi-bin/webscr?cmd=_s-xclick&hosted_button_id=YUW4CWCAABCU2)

## Table of Contents

* [The Implementation (How it works)](#the-implementation)
* [Using the Documentation Project](#using-the-documentation-project)
* [How to Contribute Code](#how-to-contribute-code)
  * [Dependencies](#dependencies)
  * [Install & Build](#install-build)
  * [Testing](#testing)
  * [Submit A Pull Request](#submit-a-pull-request)
    * [Things to Review](#things-to-review)
  * [Coding Standards](#coding-standards)
    * [File Headers](#jsdoc-file-headers)
    * [Variables](#variable-declarations)
    * [Quotes](#handling-quotes)
    * [Readability](#efficiency-readability)
    * [Comments](#comments)
    * [The Bulk of Expanded Formatting](#the-bulk-of-expanded-formatting)
    * [Look Again](#double-check)
    * [To be continued](#there-is-more)
  * [CSS Naming Conventions (external link)](https://gitlab.com/mmod/kwaeri-user-experience#css-naming-conventions)
  * [What to Do](#what-to-do)
* [Other Ways to Contribute](#other-ways-to-contribute)
  * [Bug Reports](#bug-reports)
  * [Vulnerability Reports](#vulnerability-reports)
    * [Confidential Issues](#confidential-issues)
  * [Donations](#donations)

## The Implementation

The documentation project creates a centralized location for linkable and shareable information. 

In the `README.md` file (*this document*), you'll find the briefs - or information as it would typically be included on a project basis. This includes processes for things such as code contribution, as well as information outlining coding standards, naming conventions, support processes, making bug reports, reporting vulnerabilities, etc.

All projects will still maintain their own `README.md` file, though typically we'll add/remove sections according to the project and what information applies to said project. This project is inclusive of all the information we posses between all projects.

This project also serves to host our [Massively Modified Wiki](#table-of-contents), where you'll find more in-depth information about a variety of subjects, including those covered briefly in the `README.md` file(s).

## Using the Documentation Project

Peruse this `README.md` file for basic information in a format which is brief and mostly easy to follow.

Visit the [Massively Modified Wiki](#table-of-contents) for in-depth information, guides, and more.

## Conributing to this Documentation

If you are a team member, contributing to this project is as simple as modifying this README.md file, or visiting the [Massively Modified Wiki](#table-of-contents) directly and making changes.

If you do not possess the priviledges required, you'll need to fork this project, and submit a merge request.  Please follow the guidelines as outlined below for submitting pull requests.

## How to Contribute Code

Our JavaScript projects use a Gulp work-flow for customization and/or development. If you'd like to do either, here's how to get started:

### Dependencies

Please note the following dependencies are required for building:

* Git
* Node.js
* Npm (Usually included with Node.js)

### Install & Build

The remaining dependencies are installed via npm:

```bash
git clone https://gitlab.com/mmod/gulp-bump-version
cd gulp-bump-version
npm install
```

At this point several dependencies will have installed. Once your terminal/command prompt stops spewing information, you may proceed with building the project for the first time:

```bash
gulp
```

That's it! You can open the `index.js` file located in the root of the `src` directory to work on major functionality, or the other files within the same directory to work on minor and other functionality.

You can browse the `gulpfile.babel.js` file for the tasks that are used in building the plug-in, and make use of them individually as you require in order to accomplish your goals.

There is also a `watch` task available which will run the builds automatically as changes are detected in TS and SCSS files.

### Testing

Before submitting a pull request, we require that all test pipelines pass. You can ensure that you have not broken anything by running the included test:

```bash
npm test
```

However, you should be able to add to the test.ts file depending on what you are adding, to ensure that your new bug-fix/enhancement/feature is covered in the test. If you are working on code-coverage:

```bash
npm run coverage
```

will test just that, and provide you with the feedback you need to increase code coverage.

### Submit a Pull Request

Create a fork of the project to manage your contributions with.

Clone your fork of the project to your local development machine and create a branch to work on your new feature, feature enhancement, or issue resolution, and name it by following these guidelines, separating each part by an underscore:

1. Specify the type, such as: Feature, Enhancement, Fix.
2. Specify the name of the feature - and include a description of the enhancement if that is the case, or the id and description of the issue.
3. Specify your name.
4. Specify your email.

For example:

If you are looking to submit a new feature that allows for providing custom regex, the branch should be named as follows:

`Feature_support-custom-regex_Richard-B-Winters_devrikx@gmail.com`

If you are looking to enhance the feature that supports JavaScript files so that it allows for supplying any file type:

`Enhancement_Support-all-file-types_Richard-B-Winters_devrikx@gmail.com`

If you are looking to submit a bug fix that resolves an issue with the plug-in crashing when it parses a version string with a prerelease component:

`Fix_12345_Resolve-crash-on-prerelease-bumps_Richard-B-Winters_devrikx@gmail.com`

On your new branch, proceed to implement your feature, enhancement, or issue resolution. Once finished, submit a pull request.

While you'd obviously need an account to submit a pull request, the point of the name and email is so that we can add your name and email to the authors list with out specific correspondance dedicated to asking how you'd like to be added. Please be sure to specify the name and email as you'd like to be added.

Rarely will we wholly deny a pull request, but we do have some guide-lines for the code we merge. Until the documentation is written up and publically posted, we'll be cherry-picking and making edits as necessary to pull requests prior to merging.

#### Things to Review

* Please note that enhancements and new features should be sensible and adhere to the policy of 'minimal tools for rapid development'.
* There should be no redundancy, JavaScript errors, spelling or grammatical errors.
* The code should be well tested, should come with a unit test for each method introduced, and should not cause any existing tests to fail.
* When you make the pull request, our CI/CD will run the embedded pipelines/test - of which yours should be included. We **require** that the CI/CD pipelines pass before we will review any pull request and/or merge any new code.

### Coding Standards

When writing code for any Massively Modified project, some basic guidelines should be followed.  Let us present a code snippet and then highlight some points regarding it:

```javascript
/**
 * @package:   Key Points on Expanded Formatting for Coding Standards
 * @author:    Richard B Winters <rik@mmod.co>
 * @copyright: 2018 Massively Modified, Inc.
 * @license:   Apache-2.0 (or SEE LICENSE IN license.txt)
 * @version:   0.1.0
 */

 // We're going to measure the time it takes to execute the application, create a variable store the start time:
 let startingTime       = performance.now(),
     estimatedEndTime   = startingTime + 10,
     endingTime;

// Define a variable to hold some key points on expandedFormatting:
let expandedFormatting =
{
    'what-about-it':    'That expanded formatting is awesome!?',
    interesting:        'It mostly forces coders to have a finger on each aspect of their code.',
    moreInteresting:    'It is more readable for me, when I can expect certain bits of code flow to appear naturally from top to bottom.',
    mostInteresting:    'After optimization, the code will take the least amount of space, expanded formatting is for development!'
}

...
```

#### JSDoc File Headers

* Every JavaScript file or file which supports JSDoc-style comments should have one.
  * Every key/tag in the JSDoc-style header is aligned (and valid), with left-hand and right-hand components justified, respectively.
  * The keys/tags seen in the above example are required.

#### Variable Declarations

* Variable declarations should be grouped by purpose and neatly expressed (as you'll see continued below):
  * The time stamp variables were grouped together when declared.
  * Whether initialized or simply declared, they are vertically aligned such that left-hand and right-hand components are justified, respectively.
  * As you'll see below, if a trailing declaration goes over the center-line in alignment, its mostly alright.

#### Handling Quotes

* For assignment we use single quotes.
  * It allows us to use double quotes on HTML that is embedded in strings.
  * For triply nested quotes you escape the third set - or second set of single quotes.
* For comparison purposes use double quotes, which denote 'string' in most languages - though its alright *not* to as well if it does not bother the result.
  * Double quotes are the requirement for attributes in HTML (with our standards at least) and in JavaScript you tend to generate HTML.  Therefore we like to use single quotes as often as possible in JavaScript in order to maintain consistency through out code.

Another snippet which continues to build the application we started in the first snippet:

```javascript
// Start with some general output to create a feeling of a lecture (through a console :P):
console.log( '/------------------------------------------------------------------------------*' );
console.log( 'Key Points on Expanded Formatting for Coding Standards' );
console.log( '*------------------------------------------------------------------------------/' );
console.log();

// Echo a headline for our output, followed by a blank line:
console.log( 'Did you know?' );
console.log();

// Iterate over the members of expandedFormatting and output the key-value pairs:
for( let keyPoint in expandedFormatting )
{
    // As long as our key is something we can access using dot-notation (for no apparent reason, we wont be doing so...):
    if( keyPoint !== "what-about-it" )
    {
        // Do the dirty:
        console.log( 'Something ' + keyPoint + ' about expanded formatting is: ' );
        console.log( expandedFormatting[keyPoint] );
        console.log();
    }
}

// End off the lecture with a closing statement:
console.log( expandedFormatting['what-about-it'] );

// Now let's get the current time and store it in a variable:
try
{
    // We wrapped this in a try catch block simply to show how to format control flow:
    endingTime = performance.now();
}
catch( e )
{
    console.log( 'ERROR!: ' + e.message );
}

// Let's output how long it took to run this function:
console.log( 'It was estimated that it would take ' + estimatedEndTime + ' to execute this application.' );
console.log( 'It actually took ' + ( endingTime - startingTime ) + ' milliseconds to execute this application.' );

// This line gives a great example of how parenthesis should be padded, especially when nested.
console.log( 'There was a difference of ' + ( ( endingTime - startingTime ) - estimatedEndTime ) + ' between the estimated and actual time to execute this application.' );

...
```

In the above code, we continued to use expanded formatting, let us *expand* (pun intended) upon that:

#### Efficiency & Readability

* Like the first snippet - and in the subsequent snippet(s) - we manually typed every bit of the code, and if it was auto-generated we likely had to manually format it to some extent:
  * This forces developers to pay attention to everything they are including in their code - which helps to ensure that whole statements are properly output, and inclusive of all that is required (i.e. semi-colons, usage of quotes, etc ).
* While the code takes up more space, devleopers work in developer source files, and code SHOULD take up as much space as needed to ensure:
  * That it is readable.
  * That it is well documented.
  * That it is whole and complete.

Expanding upon the above, prior to production source code is typically minified and mangled, ensuring the least amount of space is used. There is never a need to worry about the number of lines being used in developer source files - unless of course we are dealing with redundancy.

#### Comments

Now notice the commenting used in the above - and subsequent - snippet(s):

* We ensure that the code was commented, and well at that:
  * Each comment takes the developer through the process bit by bit.
  * Comments each start one space in from the double-slash, with a capital letter - and end with a semi-colon.
  * Comments are concise and to-the-point.

Let us provide yet another snippet, which shall finish off the application we've been building:

```javascript
// Check if our estimated end time was good or not, this variable flags the result of checking such:
let estimatedTimePassed = false,
    timeActuallyTaken   = ( endingTime - startingTime ),
    differenceInTime    = ( timeActuallyTaken - estimatedEndTime ),
    differenceInTimeString;

// We'll use an if/else statement to check, however - so some code flow is covered:
if( timeActuallyTaken >= estimatedEndTime  )
{
    // If it was equal, our estimation passed:
    if( timeActuallyTaken === estimatedEndTime )
    {
        estimatedTimePassed = true;
    }

    // Regardless, set a string to output to the user:
    differenceInTime = '+ ' + timeActuallyTaken - estimatedEndTime + ' milliseconds';
}
else
{
    differenceInTime = '- ' + estimatedEndTime - timeActuallyTaken + ' milliseconds';
}

switch( estimatedTimePassed )
{
    case true:
    {
        // Our estimation was correct:
        console.log( 'YEAH! The difference between our estimated time and actual time to excute this application was: ' + differenceInTimeString );
    }break;

    case false;
    {
        // Our estimation was incorrect:
        console.log( 'WHOOPSIE! The difference between our estimated time and actual time to excute this application was: ' + differenceInTimeString );

    }break;
}

```

With this last snippet we have shown all that is needed to spew at you, the reader, the next section:

#### The Bulk of Expanded Formatting

* Keywords do not have a space between their identifier and any opening parenthesis (i.e. `if()`, `else()`, `do()`, `while()`, `for()`, etc. ).
* Extending keywords such as `else`, `catch`, etc., always drop to a new line:

  ```javascript
  if( x === y )     try
  {                 {
      return true;      console.log();
  }                 }
  else              catch( exception )
  {                 {
      return false;     return false;
  }                 }
  ```

* Similarly, Control flow **always** starts on the next line, drop the opening parenthesis/bracket/brace to the next line!
* Parenthesis should be padded, add a space after the opening and before the closing parenthesis.
* Assignment operators should have a space before and after.
* Binary operators should have a space before and after.
* Unary operators should connect directly to the variable they are operating on (i.e. `++i`, `i++`, `--i`, `i--`).

#### Double Check

And try to pay attention to the following, following suite when contributing to our code(s):

* Naming, declaration, and initialization of variables.
  * Grouping of related variables, and alignment of left and right hand components of the expressions.
  * Expansion of array/objects during initialization.
* Usage and expression of comments.
* Placement of opening and closing brackets with respect to code flow and control.
* Padding of parenthesis, including nested parenthesis.
  * Please do not pad empty parentehsis.
* Usage of variables to hold/flag results of mathematical and/or comparison operations so they aren't computed multiple times.
* Required bits of a file, such as a file header and the required tags.

#### There is more

There is more to our standards, and we will touch on them more in-depth in the future. Expect to find a link to such documentation eventually.

Until then, we'll link to some files which we feel strongly represent the bulk of our coding standards, including the bits we havent touched on yet - and how to handle specific situations that may raise a flag for some with regards to how to express code in such circumstances.

### What to Do

If you really, really want to contribute, but have no idea where to begin, here are some ideas:

* You can peruse the Issues list for Feature Requests or Bugs that are in need of attention,
* You can go through documentation and/or text in the sandbox and attack syntax/grammar issues.
* You could help out with support in the Issues List
* You could go through this README.md file and help make it better, i.e. by providing language translations.

Regardless of what you're approach is, feel free to clone a copy of the repository and contribute to the code base and/or documentation as you find yourself willing to do so.

## Other Ways to Contribute

There are other ways to contribute to the project other than with code. Consider testing the software, or in case you've found an issue - please report it. You can also support the project monetarly through donations via PayPal.

### Bug Reports

To submit bug reports, request enhancements, and/or new features - please make use of the **issues** system baked-in to our source control project space at [Gitlab](https://gitlab.com/mmod/kwaeri-user-experience/issues)

You may optionally start an issue, track, and manage it via email by sending an email to our project's [support desk](mailto:incoming+mmod/kwaeri-user-experience@incoming.gitlab.com).

### Vulnerability Reports

Our Vulnerability Reporting process is very similar to Gitlab's. In fact, you could say its a *fork*.

To submit vulnerability reports, please email [Support](mailto:support@mmod.co). We will try to acknowledge receipt of said vulnerability by the next business day, and to also provide regular updates about our progress. If you are curious about the status of your report feel free to email us again. If you wish to encrypt your disclosure email, like with gitlab - please email us to ask for our GPG Key.

Please refrain from requesting compensation for reporting vulnerabilities. We will publicly acknowledge your responsible disclosure, if you request us to do so. We will also try to make the confidential issue public after the vulnerability is announced.

You are not allowed, and will not be able, to search for vulnerabilities on Gitlab.com. As our software is open source, you may download a copy of the source and test against that.

#### Confidential Issues

When a vulnerability is discovered, we create a [confidential issue] to track it internally. Security patches will be pushed to private branches and eventually merged into a `security` branch. Security issues that are not vulnerabilites can be seen on our [public issue tracker](https://gitlab.com/mmod/kwaeri-user-experience/issues?label_name%5B%5D=Security).

### Donations

If you cannot contribute time or energy to neither the code base, documentation, nor community support; please consider making a monetary contribution which is extremely useful for maintaining the Massively Modified network and all the goodies offered free to the public.

[![Donate via PayPal.com](https://gitlab.com/mmod/kwaeri-user-experience/raw/master/images/mmod-donate-btn-2.png)](https://www.paypal.com/cgi-bin/webscr?cmd=_s-xclick&hosted_button_id=YUW4CWCAABCU2)
